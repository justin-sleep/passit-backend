from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle
from rest_framework import permissions
from knox.auth import TokenAuthentication
from apps.auth.permissions import HasVerifiedEmail
from .models import OTP
from .serializers import VerifyMFASerializer, ActivateMFASerializer


class GenerateMFAView(APIView):
    def post(self, request, format=None):
        result = OTP.generate(request.user)
        return Response(result)


class ActivateMFAView(APIView):
    """ Activate an OTP object, making it begin validating authentication """
    def post(self, request, format=None):
        serializer = ActivateMFASerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        otp = get_object_or_404(OTP, pk=serializer.data['id'], user=request.user)
        otp.is_active = True
        is_valid = otp.verify(serializer.data['otp'], request.auth)
        if is_valid:
            return Response()
        return Response(status=status.HTTP_400_BAD_REQUEST)


class DeactivateMFAView(APIView):
    """ Deactivate an OTP object, making the user no longer require MFA """
    def post(self, request, format=None):
        request.user.otp_set.all().delete()
        return Response()


class Burst10MinUser(UserRateThrottle):
    rate = '10/minute'


class VerifyMFAView(APIView):
    throttle_classes = (Burst10MinUser,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated, HasVerifiedEmail,)

    def post(self, request, format=None):
        serializer = VerifyMFASerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_otp = OTP.objects.filter(is_active=True, user=request.user).first()
        if user_otp:
            is_valid = user_otp.verify(serializer.data['otp'], request.auth)
            if is_valid:
                return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

