from django.contrib import admin
from .models import Secret, SecretThrough


class SecretInline(admin.TabularInline):
    model = SecretThrough
    extra = 0


@admin.register(Secret)
class UniqueSecretAdmin(admin.ModelAdmin):
    inlines = [SecretInline]
