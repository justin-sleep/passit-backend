from rest_framework.test import APITestCase
from model_mommy import mommy

from ..models import Secret, SecretThrough
from ...access.models import User, Group, GroupUser, GroupUserInvite

class TestSecrets(APITestCase):
    def test_active_queryset(self):
        """ Make two groups, one of which is pending invite acceptance. 
        active queryset should return just one (the non pending one)
        """
        user = mommy.make(User)
        user_group = mommy.make(Group)
        user_secret = mommy.make(Secret, data={"password": "123"})
        user_secret_through = mommy.make(SecretThrough, secret=user_secret,
                                         user=user, group=user_group,
                                         data={"password": "123"})

        other_user = mommy.make(User)
        pending_group = mommy.make(Group)
        pending_user_group = mommy.make(GroupUser, user=user, key_ciphertext="abc", private_key_ciphertext="abc")
        pending_secret = mommy.make(Secret, data={"password": "123"})
        pending_user_secret_through = mommy.make(SecretThrough, secret=pending_secret,
                                         user=user, group=pending_group,
                                         data={"password": "123"},
                                         )
        pending_groupuser = mommy.make(GroupUser, 
            user=user,
            group=pending_group,
            key_ciphertext="abc",
            private_key_ciphertext="abc",
        )
        GroupUserInvite.objects.create(inviter=other_user, invitee_groupuser=pending_groupuser)

        secrets = Secret.objects.active(user)
        self.assertEqual(secrets.count(), 1)
